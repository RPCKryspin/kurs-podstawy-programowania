#include<stdio.h>
#include<stdbool.h>
#include<math.h>
#include<string.h>
#include<stdlib.h>

#define MAX 512 //max size of picture in one - MAX x MAX
#define BUFSIZE 1024 //buffor size

bool read(FILE *file);

void save(FILE *file,char *path);

void display(char *path);

void negatyw();

void progowanie(float thresholdf);

void zmianapoziomow(float whitef, float blackf);

void konturowanie();

void rozmywaniepoziome();

void rozmywaniepionowe();

void rozciaganiehistogramu();
