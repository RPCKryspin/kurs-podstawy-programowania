#include<stdio.h>
#include "obraz.h"

#define MAX 512 //max size of picture in one - MAX x MAX
#define BUFSIZE 1024 //buffor size

int main(){
	int choice=0;
	FILE *file=NULL;
	char path[BUFSIZE];
	float blackf, whitef;
	float thresholdf;
	do{
		printf(u8"Wybierz jedną z opcji:\n");
		
		printf(u8"\t1  - Wczytaj obraz\n");
		printf(u8"\t2  - Zapisz obraz\n");
		printf(u8"\t3  - Wyświetl obraz z pliku\n");
		printf(u8"\t4  - Edytuj pamiętany obraz - negatyw\n");
		printf(u8"\t5  - Edytuj pamiętany obraz - progowanie\n");
		printf(u8"\t6  - Edytuj pamiętany obraz - zmiana poziomów\n");
		printf(u8"\t7  - Edytuj pamiętany obraz - konturowanie\n");
		printf(u8"\t8  - Edytuj pamiętany obraz - rozmywanie poziome\n");
		printf(u8"\t9  - Edytuj pamiętany obraz - rozmywanie pionowe\n");
		printf(u8"\t10 - Edytuj pamiętany obraz - rozciąganie histogramu\n");
		printf(u8"\t11 - Koniec\n");
		
		printf(u8"Twój wybór to:");
		scanf("%d", &choice);

		switch(choice){
			case 1:
				printf(u8"Podaj ścieżkę do pliku, który chcesz wczytać lub jego nazwę:");
				scanf("%s", path);
				file=fopen(path,"r");
				
				if(file!=NULL){
    				if(!read(file)){
    					printf(u8"Coś jest z tym plikiem nie tak.\n");
    				}
    				else{
    					printf(u8"Wczytano pomyślnie.\n");
    				}
    				fclose(file);
  				}
				break;

			case 2:
				printf(u8"Podaj ścieżkę do pliku, w którym chcesz zapisać obrazek lub jego nazwę:");
				scanf("%s", path);
    			save(file,path);
    			printf(u8"Pomyślnie zapisano plik.\n");
				break;

			case 3:
				printf(u8"Podaj ścieżkę do pliku, który chcesz wyświetlić lub jego nazwę:");
				scanf("%s", path);
				display(path);
				break;

			case 4:
				negatyw();
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 5:
				printf(u8"Podaj próg jako procenty (0-czarny 100-biały):");
				scanf("%f", &thresholdf);
				
				progowanie(thresholdf);

				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 6:
				
				printf(u8"Podaj progi jako procenty (0-czarny 100-biały):\n");
				scanf("%f %f", &blackf, &whitef);

				zmianapoziomow(whitef, blackf);

				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 7:
				konturowanie();

				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 8:
				rozmywaniepoziome();
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 9:
				rozmywaniepionowe();
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 10:
				rozciaganiehistogramu();
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 11:
				printf(u8"No to pa pa!!\n");
				break;

			default:
				printf(u8"Nie znam tej instrukcji.\n");
				break;
		}
	}while(choice!=11);
	return 0;
}