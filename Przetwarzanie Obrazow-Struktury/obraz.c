#include<stdio.h>
#include<stdbool.h>
#include<math.h>
#include<string.h>
#include<stdlib.h>
#include"obraz.h"
#define MAX 512 //max size of picture in one - MAX x MAX
#define BUFSIZE 1024 //buffor size

typedef struct picture{
	int width;
	int height;
	int scale;
	int **pixels;
}picture;

picture obraz;

bool read(FILE *file){
	char buf[BUFSIZE];
	char ch;

	if(fgets(buf,BUFSIZE,file)==NULL) return false; //czy coś jest

	if(buf[0]!='P' || buf[1]!='2') return false; //czy czarnobiałe ze skalą szarości
  	
	//pomijamy komentarze
  	do{
  		ch=fgetc(file);
    	if(ch=='#'){
    		if(fgets(buf,BUFSIZE,file)==NULL) return false;
    	}
    	else
    		ungetc(ch,file);
  	}while(ch=='#');
  	//szerokość, wysokość, skala szarości - czy wszystko jest?
  	if(fscanf(file,"%d %d %d",&obraz.width,&obraz.height,&obraz.scale)!=3) return false;
  	
  	obraz.pixels=(int **)malloc(sizeof(int *)*obraz.height);
  	for(int c=0;c<obraz.height;c++) obraz.pixels[c]=(int*)malloc(sizeof(int)*obraz.width);
           // deklaracja dwuwymairowej tablicy dynamicznej
  	for(int i=0;i<obraz.height;i++)
    	for(int j=0;j<obraz.width;j++)
      		if(fscanf(file,"%d",&(obraz.pixels[i][j]))!=1) return false;
  	return true; 
}

void save(FILE *file,char *path){
	//otwieramy plik
	file=fopen(path,"w");
	//dodajem napis P2
	fprintf(file,"P2\n");
	//dodajem szerokość wysokość i skale szarości
	fprintf(file,"%d %d %d\n",obraz.width,obraz.height,obraz.scale);
	for(int i=0;i<obraz.height;i++){
		for(int j=0;j<obraz.width;j++){
			fprintf(file,"%d ",obraz.pixels[i][j]);
		}
		fprintf(file,"\n");
	}
	fclose(file);
}

void display(char *path) {
  	char ins[BUFSIZE];
  	strcpy(ins,"display ");
  	strcat(ins,path);
  	strcat(ins," &");
  	system(ins);
}

void negatyw(){
	for(int i=0;i<obraz.height;i++){
		for(int j=0;j<obraz.width;j++){
			obraz.pixels[i][j]=obraz.scale-obraz.pixels[i][j];
		}
	}
}

void progowanie(float thresholdf){
	int thresholdd=ceil((float)obraz.scale*thresholdf/100.0);
	for(int i=0;i<obraz.height;i++){
		for(int j=0;j<obraz.width;j++){
			if(obraz.pixels[i][j]<=thresholdd)obraz.pixels[i][j]=0;
			else obraz.pixels[i][j]=obraz.scale;
		}
	}
}

void zmianapoziomow(float whitef, float blackf){
	if(whitef < blackf){
		float temp=blackf;
		blackf=whitef;
		whitef=temp;
	}
	int whited=floorf(whitef/100.0*(float)obraz.scale);
	int blackd=ceilf(blackf/100.0*(float)obraz.scale);

	for(int i=0;i<obraz.height;i++){
		for(int j=0;j<obraz.width;j++){
			if(obraz.pixels[i][j]<=blackd)obraz.pixels[i][j]=0;
			else if(obraz.pixels[i][j]>=whited)obraz.pixels[i][j]=obraz.scale;
			else{
				obraz.pixels[i][j]-=blackd;
				obraz.pixels[i][j]*=obraz.scale;
				obraz.pixels[i][j]/=(whited-blackd);
			}
		}
	}
}

void konturowanie(){
	for(int i=0;i<obraz.height-1;i++){
		for(int j=0;j<obraz.width-1;j++){
			obraz.pixels[i][j]=abs(obraz.pixels[i+1][j]-obraz.pixels[i][j])+abs(obraz.pixels[i][j+1]-obraz.pixels[i][j]);
		}
	}
}

void rozmywaniepoziome(){
	for(int i=0;i<obraz.height;i++){
		int ost=obraz.pixels[i][0], ost2;
		for(int j=1;j<obraz.width-1;j++){
			ost2=obraz.pixels[i][j];
			obraz.pixels[i][j]=(obraz.pixels[i][j]+obraz.pixels[i][j+1]+ost)/3;
			ost=ost2;
		}
	}
}
void rozmywaniepionowe(){
	for(int j=0;j<obraz.width;j++){
		int ost=obraz.pixels[0][j], ost2;
		for(int i=1;i<obraz.height-1;i++){
			ost2=obraz.pixels[i][j];
			obraz.pixels[i][j]=(obraz.pixels[i][j]+obraz.pixels[i+1][j]+ost)/3;
			ost=ost2;
		}
	}
}

void rozciaganiehistogramu(){
	int white=0;
	int black=obraz.scale;

	for(int i=0;i<obraz.height;i++){
		for(int j=0;j<obraz.width;j++){
			if(obraz.pixels[i][j]>white)white=obraz.pixels[i][j];
			if(obraz.pixels[i][j]<black)black=obraz.pixels[i][j];
		}
	}

	for(int i=0;i<obraz.height;i++){
		for(int j=0;j<obraz.width;j++){
			if(obraz.pixels[i][j]<=black)obraz.pixels[i][j]=0;
			else if(obraz.pixels[i][j]>=white)obraz.pixels[i][j]=obraz.scale;
			else{
				obraz.pixels[i][j]-=black;
				obraz.pixels[i][j]*=obraz.scale;
				obraz.pixels[i][j]/=(white-black);
			}
		}
	}
}
