#include<stdio.h>
#include"pliki_pgm.h"

/************************************************************************************
 * Funkcja wczytuje obraz PGM z pliku do struktury     	       	       	       	    *
 *										    *
 * \param[in] plik_we uchwyt do pliku z obrazem w formacie PGM			    *
 * \param[out] obraz struktura, do ktorej zostanie wczytany obraz		    *
 * \return liczba wczytanych pikseli						    *
 ************************************************************************************/

int czytaj_pgm(FILE *plik_we, t_obraz *obraz) {
  
  printf("Wczytuje...\n");      /* Tu sie dzieje co trzeba */
  return 0; 
}

/************************************************************************************
 * Funkcja zapisuje obraz ze struktury do pliku w formacie PGM                      *
 *										    *
 * \param[out] plik_wy uchwyt do zapisywanego pliku      			    *
 * \param[in] obraz struktura, z ktorej zostanie zapisany obraz	    	            *
 * \return liczba zapisanych pikseli						    *
 ************************************************************************************/

int zapisz_pgm(FILE *plik_wy, t_obraz *obraz) {
  
  printf("Zapisuje...\n");      /* Tu sie dzieje co trzeba */
  return 0; 
}
