#include<stdio.h>
#include"pliki_pgm.h"
#include"filtry.h"

int main() {
  t_obraz obraz;
  FILE *plik_we=stdin, *plik_wy=stdout;

  czytaj_pgm(plik_we, &obraz); /* Wczytanie zawartosci pliku do pamieci */
  negatyw(&obraz);             /* Wyliczenie negatywu */
  zapisz_pgm(plik_wy, &obraz); /* Zapisanie negatywu do pliku */
  return 0;
}
