#include<stdio.h>
#include<string.h>
#include<stdbool.h>
#include<math.h>
#include<stdlib.h>

#define MAX 512 //max size of picture in one - MAX x MAX
#define BUFSIZE 1024 //buffor size

bool read(FILE *file, int picture[][MAX],int *wid, int *hei, int *scale){
	char buf[BUFSIZE];
	char ch;

	if(fgets(buf,BUFSIZE,file)==NULL) return false; //czy coś jest

	if(buf[0]!='P' || buf[1]!='2') return false; //czy czarnobiałe ze skalą szarości
  	
	//pomijamy komentarze
  	do{
  		ch=fgetc(file);
    	if(ch=='#'){
    		if(fgets(buf,BUFSIZE,file)==NULL) return false;
    	}
    	else
    		ungetc(ch,file);
  	}while(ch=='#');
  	//szerokość, wysokość, skala szarości - czy wszystko jest?
  	if(fscanf(file,"%d %d %d",wid,hei,scale)!=3) return false;
    //czy wszystkie piksele są?
  	for(int i=0;i<*hei;i++)
    	for(int j=0;j<*wid;j++)
      		if(fscanf(file,"%d",&(picture[i][j]))!=1) return false;
  	return true; 
}

void save(FILE *file,int picture[][MAX],char *path,int wid, int hei, int scale){
	//otwieramy plik
	file=fopen(path,"w");
	//dodajem napis P2
	fprintf(file,"P2\n");
	//dodajem szerokość wysokość i skale szarości
	fprintf(file,"%d %d %d\n",wid,hei,scale);
	for(int i=0;i<hei;i++){
		for(int j=0;j<wid;j++){
			fprintf(file,"%d ",picture[i][j]);
		}
		fprintf(file,"\n");
	}
	fclose(file);
}

void display(char *path) {
  	char ins[BUFSIZE];
  	strcpy(ins,"display ");
  	strcat(ins,path);
  	strcat(ins," &");
  	system(ins);
}

void negatyw(int height, int width, int scale, int picture[][MAX]){
	for(int i=0;i<height;i++){
		for(int j=0;j<width;j++){
			picture[i][j]=scale-picture[i][j];
		}
	}
}

void progowanie(int scale, float thresholdf, int height, int width, int picture[][MAX]){
	int thresholdd=ceil((float)scale*thresholdf/100.0);
	for(int i=0;i<height;i++){
		for(int j=0;j<width;j++){
			if(picture[i][j]<=thresholdd)picture[i][j]=0;
			else picture[i][j]=scale;
		}
	}
}

void zmianapoziomow(float whitef, float blackf, int height, int width, int scale, int picture[][MAX]){
	if(whitef < blackf){
		float temp=blackf;
		blackf=whitef;
		whitef=temp;
	}
	int whited=floorf(whitef/100.0*(float)scale);
	int blackd=ceilf(blackf/100.0*(float)scale);

	for(int i=0;i<height;i++){
		for(int j=0;j<width;j++){
			if(picture[i][j]<=blackd)picture[i][j]=0;
			else if(picture[i][j]>=whited)picture[i][j]=scale;
			else{
				picture[i][j]-=blackd;
				picture[i][j]*=scale;
				picture[i][j]/=(whited-blackd);
			}
		}
	}
}

void konturowanie(int height, int width, int picture[][MAX]){
	for(int i=0;i<height-1;i++){
		for(int j=0;j<width-1;j++){
			picture[i][j]=abs(picture[i+1][j]-picture[i][j])+abs(picture[i][j+1]-picture[i][j]);
		}
	}
}

void rozmywaniepoziome(int height, int width, int picture[][MAX]){
	for(int i=0;i<height;i++){
		int ost=picture[i][0], ost2;
		for(int j=1;j<width-1;j++){
			ost2=picture[i][j];
			picture[i][j]=(picture[i][j]+picture[i][j+1]+ost)/3;
			ost=ost2;
		}
	}
}
void rozmywaniepionowe(int height, int width, int picture[][MAX]){
	for(int j=0;j<width;j++){
		int ost=picture[0][j], ost2;
		for(int i=1;i<height-1;i++){
			ost2=picture[i][j];
			picture[i][j]=(picture[i][j]+picture[i+1][j]+ost)/3;
			ost=ost2;
		}
	}
}

void rozciaganiehistogramu(int height, int width, int scale, int picture[][MAX]){
	int white=0;
	int black=scale;

	for(int i=0;i<height;i++){
		for(int j=0;j<width;j++){
			if(picture[i][j]>white)white=picture[i][j];
			if(picture[i][j]<black)black=picture[i][j];
		}
	}

	for(int i=0;i<height;i++){
		for(int j=0;j<width;j++){
			if(picture[i][j]<=black)picture[i][j]=0;
			else if(picture[i][j]>=white)picture[i][j]=scale;
			else{
				picture[i][j]-=black;
				picture[i][j]*=scale;
				picture[i][j]/=(white-black);
			}
		}
	}
}

int main(){
	int picture[MAX][MAX];
	int height, width, scale;
	int choice=0;
	FILE *file=NULL;
	char path[BUFSIZE];
	float blackf, whitef;
	float thresholdf;

	do{
		printf(u8"Wybierz jedną z opcji:\n");
		
		printf(u8"\t1  - Wczytaj obraz\n");
		printf(u8"\t2  - Zapisz obraz\n");
		printf(u8"\t3  - Wyświetl obraz z pliku\n");
		printf(u8"\t4  - Edytuj pamiętany obraz - negatyw\n");
		printf(u8"\t5  - Edytuj pamiętany obraz - progowanie\n");
		printf(u8"\t6  - Edytuj pamiętany obraz - zmiana poziomów\n");
		printf(u8"\t7  - Edytuj pamiętany obraz - konturowanie\n");
		printf(u8"\t8  - Edytuj pamiętany obraz - rozmywanie poziome\n");
		printf(u8"\t9  - Edytuj pamiętany obraz - rozmywanie pionowe\n");
		printf(u8"\t10 - Edytuj pamiętany obraz - rozciąganie histogramu\n");
		printf(u8"\t11 - Koniec\n");
		
		printf(u8"Twój wybór to:");
		scanf("%d", &choice);

		switch(choice){
			case 1:
				printf(u8"Podaj ścieżkę do pliku, który chcesz wczytać lub jego nazwę:");
				scanf("%s", path);
				file=fopen(path,"r");
				
				if(file!=NULL){
    				if(!read(file,picture,&width,&height,&scale)){
    					printf(u8"Coś jest z tym plikiem nie tak.\n");
    				}
    				else{
    					printf(u8"Wczytano pomyślnie.\n");
    				}
    				fclose(file);
  				}
				break;

			case 2:
				printf(u8"Podaj ścieżkę do pliku, w którym chcesz zapisać obrazek lub jego nazwę:");
				scanf("%s", path);
    			save(file,picture,path,width,height,scale);
    			printf(u8"Pomyślnie zapisano plik.\n");
				break;

			case 3:
				printf(u8"Podaj ścieżkę do pliku, który chcesz wyświetlić lub jego nazwę:");
				scanf("%s", path);
				display(path);
				break;

			case 4:
				negatyw(height, width, scale, picture);
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 5:
				printf(u8"Podaj próg jako procenty (0-czarny 100-biały):");
				scanf("%f", &thresholdf);
				
				progowanie(scale, thresholdf, height, width, picture);

				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 6:
				
				printf(u8"Podaj progi jako procenty (0-czarny 100-biały):\n");
				scanf("%f %f", &blackf, &whitef);

				zmianapoziomow(whitef, blackf, height, width, scale, picture);

				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 7:
				konturowanie(height, width, picture);

				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 8:
				rozmywaniepoziome(height, width, picture);
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 9:
				rozmywaniepionowe(height, width, picture);
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 10:
				rozciaganiehistogramu(height, width, scale, picture);
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 11:
				printf(u8"No to pa pa!!\n");
				break;

			default:
				printf(u8"Nie znam tej instrukcji.\n");
				break;
		}
	}while(choice!=11);
	return 0;
}