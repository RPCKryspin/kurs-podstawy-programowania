/* Krystian Pacyniak 253991 Przetwarzanie Obrazow 1  9-10 stycznia 2020*/

/*Testy i Wnioski na Końcu. Troche o Działaniu i użytkowaniu. Działam na prostym menu wyboru. aby poprawnie używać aplikacji należy pamiętać o pierwszorzędnym wczytaniu obrazu po czym można przejść do edycji po czym można obraz zapisać jako nowy plik po czym wyświetlić po podaniu ścieżki. nie wyświetla się gotowy obraz z zapisem tylko po podaniu ścieżki do nowo powstałego pliku. Jednakże należy pamiętać że OBRAZ po np negatywnie bedzie w pamiecie jako negatyw a po uzyciu kolejnej edycji np rozmycie otrzymamy rozmycie negatywu. Dlatego należy pamiętać o ponownym wczytaniu pierwotnego zdjęcia w celu jego zasadniczej obróbki */

#include<stdio.h>
#include<string.h>
#include<stdbool.h>
#include<math.h>
#include<stdlib.h>

#define MAX 512 					/* Maksymalny rozmiar wczytywanego obrazu */
#define BUFSIZE 1024 					/* Dlugosc buforow pomocniczych */

							/* file - uchwyt do pliku z obrazem w formacie PGM (uchwyt) -gupio brzmi rzekłbym czy porgram	
 							picture -tablica, do ktorej zostanie zapisany obraz formatu pgm	
  							wid - szerokosc obrazka wymy - hei obrazka scale -liczba odcieni szarosci*/

bool read(FILE *file, int picture[][MAX],int *wid, int *hei, int *scale){     /*Wczytywanie*/
							/*Funkcja wczytuje obraz PGM z pliku do tablicy za pomocą zmiennej bool stąd true lub false*/  
	char buf[BUFSIZE];  				/* bufor pomocniczy do czytania naglowka i komentarzy */
	char ch;            				 /* ch jako znak od char zarazem zmienna pomocnicza do czytania komentarzy */

	if(fgets(buf,BUFSIZE,file)==NULL) return false; /*Wczytanie pierwszej linii pliku do bufora  i sprawdzenie czy coś jest jak nie ma to koniec wczytywania*/

	if(buf[0]!='P' || buf[1]!='2') return false; 	/*czy czarnobiałe ze skalą szarości czyli P2 pliki pgm*/
  	
							/*pomijamy komentarze*/
  	do{
  		ch=fgetc(file);
    	if(ch=='#'){    				 /* Czy linia rozpoczyna sie od znaku '#'? */
    		if(fgets(buf,BUFSIZE,file)==NULL) return false;  /* Przeczytaj ja do bufora  */
    	}
    	else
    		ungetc(ch,file);                            /* Gdy przeczytany znak z poczatku linii  nie jest '#' zwroc go */
  	}while(ch=='#'); 				/* Powtarzaj dopoki sa linie komentarza */
  							/*szerokość, wysokość, skala szarości - czy wszystko jest */
  	if(fscanf(file,"%d %d %d",wid,hei,scale)!=3) return false;
    							/*czy wszystkie piksele są */
  	for(int i=0;i<*hei;i++)                         /* Pobranie obrazu i zapisanie w tablicy całego file*/
    	for(int j=0;j<*wid;j++)
      		if(fscanf(file,"%d",&(picture[i][j]))!=1) return false; 	/*sprawdzenie wymiarów obrazu*/
  	return true;  						/* Czytanie zakonczone sukcesem    */
}

void save(FILE *file,int picture[][MAX],char *path,int wid, int hei, int scale){   /*Zapisywanie*/
									/* otwiermy plik ,a path czyli ścieżka do pliku */
	file=fopen(path,"w");
									/*dodajem napis P2 */         
	fprintf(file,"P2\n");
								/*dodajem szerokość wysokość i skale szarości */
	fprintf(file,"%d %d %d\n",wid,hei,scale);
	for(int i=0;i<hei;i++){                                   
		for(int j=0;j<wid;j++){
			fprintf(file,"%d ",picture[i][j]);    /*przepisujemy piksele (dane binarne) do nowego pliku tworząc nowy obraz etc */
		}
		fprintf(file,"\n");
	}
	fclose(file);
}

void display(char *path) {     				/* Wyświetlanie */
  	char ins[BUFSIZE];  				 /* bufor pomocniczy do zestawienia polecenia instrukcja-ins */
  	strcpy(ins,"display "); 			/* konstrukcja polecenia postaci */
  	strcat(ins,path);                               /* display "nazwa_pliku(ścieżka) &       */
  	strcat(ins," &");                                 /* wydruk kontrolny polecenia */
  	system(ins);                                       /* wykonanie polecenia        */
}

int main(){
	int picture[MAX][MAX];                     
	int height, width, scale;
	int choice=0;    					/*wybór do meni*/
	FILE *file=NULL;
	char path[]=" ";
	int blackd, whited;
	int black, white;
	float blackf, whitef;   /* zmienne pomocnicze d dla inta f dla floata itd zmiennoprzeickowe dla procentów itd */
	float thresholdf;

	do{   						/* Opcje Menu*/   /* w moim porgramie funkcje edycja są w case'ach */
		printf(u8"Wybierz jedną z opcji:\n");
					
		printf(u8"\t1  - Wczytaj obraz\n");	/* u8 - polska pisownia \t- tabulacja oraz nowo użyta funkcja do while rób, a potem jeśli pętla do której wraca*/
		printf(u8"\t2  - Zapisz obraz\n");
		printf(u8"\t3  - Wyświetl obraz z pliku\n");
		printf(u8"\t4  - Edytuj pamiętany obraz - negatyw\n");
		printf(u8"\t5  - Edytuj pamiętany obraz - progowanie\n");
		printf(u8"\t6  - Edytuj pamiętany obraz - zmiana poziomów\n");
		printf(u8"\t7  - Edytuj pamiętany obraz - konturowanie\n");
		printf(u8"\t8  - Edytuj pamiętany obraz - rozmywanie poziome\n");
		printf(u8"\t9  - Edytuj pamiętany obraz - rozmywanie pionowe\n");
		printf(u8"\t10 - Edytuj pamiętany obraz - rozciąganie histogramu\n");
		printf(u8"\t11 - Koniec\n");
		
		printf(u8"Twój wybór to:");
		scanf("%d", &choice);

		switch(choice){
			case 1:
				printf(u8"Podaj ścieżkę do pliku, który chcesz wczytać lub jego nazwę:");
				scanf("%s", path);
				file=fopen(path,"r");  				/* Wczytanie zawartosci wskazanego pliku do pamieci */
				
				if(file!=NULL){  				/* co spowoduje zakomentowanie tego warunku */
    				if(!read(file,picture,&width,&height,&scale)){
    					printf(u8"Coś jest z tym plikiem nie tak.\n");
    				}
    				else{
    					printf(u8"Wczytano pomyślnie.\n");
    				}
    				fclose(file);
  				}
				break;

			case 2:
				printf(u8"Podaj ścieżkę do pliku, w którym chcesz zapisać obrazek lub jego nazwę:");
				scanf("%s", path); 			/*Zapisanie obrazu jako nowy plik zdomyślnym rozszerzeniem pgm*/
    			save(file,picture,path,width,height,scale);
    			printf(u8"Pomyślnie zapisano plik.\n");
				break;

			case 3:
				printf(u8"Podaj ścieżkę do pliku, który chcesz wyświetlić lub jego nazwę:");  
				scanf("%s", path);				/* Wyswietlenie poprawnie wczytanego obraza zewnetrznym programem */
				display(path);					/* więcej uwaga co do specyfikacji i wczytywaniu i wyświetlania na końcu */
				break;

			case 4:
				for(int i=0;i<height;i++){
					for(int j=0;j<width;j++){
						picture[i][j]=scale-picture[i][j];   /*zmiana wart na przeciwna wzgledem max wart*/
					}
				}
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 5:
				printf(u8"Podaj próg jako procenty (0-czarny 100-biały):");  /*zmienia obraz na czarno bialy,okreslamy ilosc procentowo*/
				scanf("%f", &thresholdf);  					/*próg*/
				int thresholdd=ceil((float)scale*thresholdf/100.0);

				for(int i=0;i<height;i++){
					for(int j=0;j<width;j++){
						if(picture[i][j]<=thresholdd)picture[i][j]=0;
						else picture[i][j]=scale;     /*wyniki po progowaniu*/
					}
				}
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 6:
				
				printf(u8"Podaj progi jako procenty (0-czarny 100-biały):\n");
				scanf("%f %f", &blackf, &whitef);           /*okreslamy procentowo ilsoc czarnego i bialego*/

				if(whitef < blackf){
					float temp=blackf;   /* warunek ustawia w razie gdy użytkownik pomyli kolejność że czarny z białym są zamieniane */
					blackf=whitef;
					whitef=temp;
				}
				whited=floorf(whitef/100.0*(float)scale);   /*floorf bierzezaokrągle nie w dół całkowitej */
				blackd=ceilf(blackf/100.0*(float)scale);  /* ceilf zaokrąglenie do góry całkowitej */
										/* tworzenie granic do odcieni*/
				for(int i=0;i<height;i++){
					for(int j=0;j<width;j++){
						if(picture[i][j]<=blackd)picture[i][j]=0;
						else if(picture[i][j]>=whited)picture[i][j]=scale;
						else{
							picture[i][j]-=blackd;
							picture[i][j]*=scale;
							picture[i][j]/=(whited-blackd); /*wzór na zmiane poziomów */
						}
					}
				}
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 7:
				for(int i=0;i<height-1;i++){
					for(int j=0;j<width-1;j++){
						picture[i][j]=abs(picture[i+1][j]-picture[i][j])+abs(picture[i][j+1]-picture[i][j]);
					}    					/*wzór na konturowanie z wartością bezwzględną*/
				}
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 8:
				for(int i=0;i<height;i++){
					int ost=picture[i][0], ost2;
					for(int j=1;j<width-1;j++){
						ost2=picture[i][j];
						picture[i][j]=(picture[i][j]+picture[i][j+1]+ost)/3; /*wzór na rozmycie poziome */
						ost=ost2;
					}
				}
				printf(u8"Edytowano pomyślnie.\n");      	/*rozmycia ost-ostrość */
				break;

			case 9:
				for(int j=0;j<width;j++){
					int ost=picture[0][j], ost2;      
					for(int i=1;i<height-1;i++){
						ost2=picture[i][j];
						picture[i][j]=(picture[i][j]+picture[i+1][j]+ost)/3; /*wzór na rozmycie pionowe */
						ost=ost2;
					}
				}
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 10:
				white=0;
				black=scale;
				for(int i=0;i<height;i++){
					for(int j=0;j<width;j++){
						if(picture[i][j]>white)white=picture[i][j];
						if(picture[i][j]<black)black=picture[i][j];  /*badanie szarosci */
					}
				}

				for(int i=0;i<height;i++){
					for(int j=0;j<width;j++){
						if(picture[i][j]<=black)picture[i][j]=0;
						else if(picture[i][j]>=white)picture[i][j]=scale;
						else{
							picture[i][j]-=black;
							picture[i][j]*=scale;            /*wzor na rozciaganie histogramu*/
							picture[i][j]/=(white-black);
						}
					}
				}
				printf(u8"Edytowano pomyślnie.\n");
				break;

			case 11:
				printf(u8"Koniec\n");
				break;

			default:
				printf(u8"Nie znam tej instrukcji.\n");
				break;
		}
	}while(choice!=11);
}


/* obrazach w formacie pgm cóż tu się nie da ich pokazać trzeba je przeporowadzić na własne oczy należy pamiętać o wczytywaniu obrazu po każdej edycji jeśli się chce od nowa zrobić inną edycje na pierwotnym obrazie. Program działa czasem wyrzuca informacje o zrzucie pamięci kończąc program , smash (...) coś  wyświetlanie u mnie jest nie sprawne bo sudo apt blokuje mi dostęp do pobranie tzn. stare hasło nie działo a zostało wyłączone więc. A reszta dokonuje te same efekty co w pliku pdf co do filtracji z kubusiem puchatkiem. Jednak filtry na różnych obrazach potrafią różnie działać np a napisie FILE na czarnym tle potrafił inaczej zrobić obraz niż z jasnym obrazkiem z jakąś postacią Wszytkie obrazy pomyśli sięprzetworzył jedynie ni jestem pewny konturowania.  Ale co mało się wyostrzyć rozmyć wyczernić zniknąćw ybielić się zrobiło*/


/* Wnioski - porgramowanie boli potrafi być chaotyczne przy dużym projekcie. Jest całkiem okej aplikacja dla pozytywistycznego fotojokera */
